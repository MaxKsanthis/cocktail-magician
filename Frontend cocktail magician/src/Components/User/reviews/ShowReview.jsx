import Container from 'react-bootstrap/Container';
import PropTypes from 'prop-types';
import React from 'react';
import Col from 'react-bootstrap/Col';
import { useEffect, useState, useContext } from 'react';
import fetchRequest from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';
import CreateReview from './CreateReview';
import Accordion from 'react-bootstrap/esm/Accordion';
import { Link } from 'react-router-dom';
import { AuthContext } from '../../../context/auth-context';

const ShowReviews = ({ id }) => {
  const [reviews, setReviews] = useState([]);
  const { isLoggedIn } = useContext(AuthContext);

  useEffect(() => {

    const args = {
      path: `bars/${id}/reviews`,
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: data =>{
        setReviews(data);
      }
    };

    fetchRequest(args);
  }, [id]);


  return (
    isLoggedIn?
      <>
        {reviews.length ?
          <>
            <Container className="heading">
              <h2> User Reviews:</h2>
              {reviews.map(review => {
                return (
                  <Col key={review.id}>
                    <Link style={{ textDecoration: 'none' }} to={`../profile/${review.author.id}`}>{review.author.displayName}</Link>
                    <p>"{review.text}"</p>

                  </Col>
                );

              })}
            </Container>
          </> :
          <Container className="no-reviews-msg align-self-center">
            <h4> There are no reviews for this bar yet! Be the first to write one.</h4>
            <Accordion defaultActiveKey="1" flush>
              <Accordion.Item eventKey="0">
                <Accordion.Button>
                  <h5>Write review</h5>
                </Accordion.Button>
                <Accordion.Body>
                  <CreateReview id={id} setReviews={setReviews} />
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </Container>}
      </>:null
  );
};

ShowReviews.propTypes = {
  id: PropTypes.string,
};
export default ShowReviews;
