import React, { useState } from 'react';
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { getToken } from '../../../context/auth-context';
import PropTypes from 'prop-types';
import fetchRequest from '../../../requests/server-requests';

import Box from '@mui/material/Box';
import Rating from '@mui/material/Rating';

const CreateReview = ({ id, setReviews }) => {

  const [form, setForm] = useState({});
  const setField = (field, value) => {
    form[field] = value;
    setForm(form);
  };


  const onSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();

    const args = {
      path: `bars/${id}/reviews`,
      method: 'POST',
      body: JSON.stringify(form),
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: response => {
        console.log(form);
        console.log(response);
        const argsForReviews = {
          path: `bars/${id}/reviews`,
          headers: {
            'Content-type': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
          },
          handler: data =>{
            setReviews(data);
          }
        };
        fetchRequest(argsForReviews);

        if (response.message) {
          throw new Error(response.message);

        }

      }
    };

    fetchRequest(args);
  };

  return (
    <>
      <Form className="create-review-form">

        <FloatingLabel controlId="rating" className="mb-3">
          <Box
            sx={{
              '& > legend': { mt: 2 },
            }}
          />
          <Rating
            name="simple-controlled"
            onChange={(event) => {
              setField('rating', +event.target.value);
            }}
          />
        </FloatingLabel>

        <FloatingLabel controlId="floatingTextarea2" label="Leave a review here">
          <Form.Control
            required
            minLength={25}
            maxLength={1000}
            as="textarea"
            placeholder="Leave a comment here"
            style={{ height: '100px' }}
            onChange={e => setField('text', e.target.value)}
          />
          <Form.Control.Feedback type='invalid'>
            Review must be between 25-1000 characters long!
          </Form.Control.Feedback>
        </FloatingLabel>


        <Button style={{ margin: '10px auto' }} variant="dark" type="submit" onClick={onSubmit}>
          Submit
        </Button>
      </Form>
    </>
  );
};

CreateReview.propTypes = {
  id: PropTypes.number,
  setReviews: PropTypes.func
};

export default CreateReview;
