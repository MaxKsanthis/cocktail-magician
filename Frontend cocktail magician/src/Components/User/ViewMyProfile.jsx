import React, { useContext, useState } from 'react';
import Container from 'react-bootstrap/Container';
import { AuthContext } from '../../context/auth-context';
import Accordion from 'react-bootstrap/esm/Accordion';
import { getToken } from '../../context/auth-context';
import fetchRequest from '../../requests/server-requests';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const ViewMyProfile = () => {

  const { user } = useContext(AuthContext);
  const [displayName, setDisplayName]= useState(user.displayName);
  const [form, setForm] = useState({ displayName: '' });

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const updateDisplayName = (event) => {
    event.preventDefault();
    event.stopPropagation();
    console.log(displayName);
    const args = {
      path: 'users',
      method: 'PUT',
      body: JSON.stringify(form),
      headers: {
        'Authorization': `Bearer ${getToken()}`,
        'Content-Type': 'application/json'
      },
      handler: (response)=> {
        setDisplayName(response.displayName);
        setField('displayName', '');
      },
      errorHandler: (err)=> console.log(err), };

    fetchRequest(args);
  };

  return (
    <Container fluid>
      <Card style={{ width: '24rem' }}>
        <Card.Img
          variant="top"
          src={user.role==='crawler'?
            'https://media.istockphoto.com/photos/knock-out-on-bar-counter-picture-id1136156913?k=20&m=1136156913&s=612x612&w=0&h=tihysdYBH6iOjBBSjM3TP9ySAxd8RVqcxqnFfq2u6lE=' :
            'https://static.designmynight.com/uploads/2019/08/TEquila-Mockingbird-optimised.jpg'}
        />
        <Card.Body>
          <Card.Title>{user.displayName ? <h4>{displayName}</h4> : <h4>no display name at the moment</h4>}</Card.Title>
          <Card.Text>
            Username: {user.username} <br />
            Role: {user.role}
          </Card.Text>
        </Card.Body>
      </Card>
      <h4>Reviews:{user.reviewedBars.map(review=>{
        return (
          <Container key={review.id}>
            <h5>Bar: {review.name}</h5>
            <h6>"{review.text}"</h6>
            <h6>Rating: {review.myRating}</h6>
          </Container>
        );
      })}
      </h4>
      <Accordion defaultActiveKey="1" flush>
        <Accordion.Item eventKey="0">
          <Accordion.Button>
            <h5>Update User</h5>
          </Accordion.Button>
          <Accordion.Body>
            <Form onSubmit={updateDisplayName} autoComplete="off">
              <Form.Group className="mb-3" controlId="formBasicUsername">
                <Form.Control
                  value={form.displayName} type="text" placeholder="Change display name"
                  required minLength={3} maxLength={30}
                  onChange={e => setField('displayName', e.target.value)}
                />
                <Form.Control.Feedback type='invalid'>
                  Invalid Display Name! Display Name should be between 3 and 30 characters long!
                </Form.Control.Feedback>
              </Form.Group>

              <Button onClick={updateDisplayName}>
                Update
              </Button>
            </Form>
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>

    </Container>
  );
};

export default ViewMyProfile;
