import React, { useEffect, useState } from 'react';
import Container from 'react-bootstrap/Container';
import fetchRequest from '../../requests/server-requests';
import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const ViewOtherProfile = (props) => {
  const [user, setUser] = useState({ username: '', role: '', displayName: '', reviewedBars: [] });
  const userId=props.match.params.id;
  const back = () => props.history.goBack();

  useEffect(() =>{
    const args = {
      path: `users/${userId}`,
      handler: data =>{
        console.log(data);
        setUser(data);
      }
    };

    fetchRequest(args);
  }, []);

  return (
    user.username!=='' ?
      <Container fluid>
        <Card style={{ width: '24rem' }}>
          <Card.Img
            variant="top"
            src={user.role==='crawler'?
              'https://media.istockphoto.com/photos/knock-out-on-bar-counter-picture-id1136156913?k=20&m=1136156913&s=612x612&w=0&h=tihysdYBH6iOjBBSjM3TP9ySAxd8RVqcxqnFfq2u6lE=' :
              'https://static.designmynight.com/uploads/2019/08/TEquila-Mockingbird-optimised.jpg'}
          />
          <Card.Body>
            <Card.Title>{user.displayName ? <h4>{user.displayName}</h4> : <h4>no display name at the moment</h4>}</Card.Title>
            <Card.Text>
              Username: {user.username} <br />
              Role: {user.role}
            </Card.Text>
          </Card.Body>
        </Card>
        <h4>Reviews:{user.reviewedBars.map(review=>{
          return (
            <Container key={review.id}>
              <h5>Bar: {review.name}</h5>
              <h6>"{review.text}"</h6>
              <h6>Rating: {review.myRating}</h6>
            </Container>
          );
        })}
        </h4>
        <Button onClick={back}>Back</Button>
      </Container>:<h3>Loading</h3>
  );
};

ViewOtherProfile.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
};

export default ViewOtherProfile;
