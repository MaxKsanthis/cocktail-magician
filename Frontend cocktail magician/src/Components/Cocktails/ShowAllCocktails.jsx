import React, { useState, useEffect, useContext } from 'react';
import CreateCocktail from '../Admin/Cocktails/CreateCocktail';
import { AuthContext } from '../../context/auth-context';
import Accordion from 'react-bootstrap/esm/Accordion';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';
import fetchRequest from '../../requests/server-requests';

const ShowAllCocktails = ({ history }) =>{

  const { user, isLoggedIn } = useContext(AuthContext);
  const [cocktails, setCocktails] = useState([]);

  useEffect(() =>{
    const args = {
      path: `cocktails`,
      handler: data =>{
        setCocktails(data);
        console.log(cocktails);
      }
    };

    fetchRequest(args);
  }, []);

  const routeChange = (id) =>{
    const path = `/cocktails/${id}`;
    history.push(path);
  };

  return (
    <>
      {isLoggedIn && user.role==='magician'?
        <Accordion defaultActiveKey="1" flush>
          <Accordion.Item eventKey="0">
            <Accordion.Button>
              <h5>Create cocktail</h5>
            </Accordion.Button>
            <Accordion.Body>
              <CreateCocktail />
            </Accordion.Body>
          </Accordion.Item>
        </Accordion> :
        null}
      {cocktails.length!==0 ?
        <Row>
          {cocktails.map(cocktail =>{
            return (
              <Col key={cocktails.id}>
                <Container>
                  <Card style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={`http://localhost:5000/images/${cocktail.imageUrl}`} />
                    <Card.Body>
                      <Card.Title>{cocktail.name}</Card.Title>
                      <Button onClick={() => routeChange(cocktail.id)}>
                        Details
                      </Button>
                    </Card.Body>
                  </Card>
                </Container>
              </Col>
            );
          })}
        </Row>:
        <h3>Loading</h3>}
    </>
  );

};

ShowAllCocktails.propTypes = {
  history: PropTypes.object,
};

export default ShowAllCocktails;
