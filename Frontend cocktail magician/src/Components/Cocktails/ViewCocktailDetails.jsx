import React from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import fetchRequest from '../../requests/server-requests';
import AddRemoveCocktail from '../Admin/Cocktails/AddRemoveCocktail';
import UpdateCocktail from '../Admin/Cocktails/UpdateCocktail';
import Accordion from 'react-bootstrap/esm/Accordion';
import AddRemoveIngredients from '../Admin/Cocktails/AddRemoveIngredients';
import { AuthContext } from '../../context/auth-context';

const ViewCocktailDetails = (props) =>{
  const id = props.match.params.id;
  const [cocktail, setCocktail] = useState({});
  const { user, isLoggedIn } = useContext(AuthContext);
  const back = () => props.history.goBack();

  useEffect(() =>{
    const args = {
      path: `cocktails/${id}`,
      handler: data =>{
        setCocktail(data);
        console.log(data);
      }
    };

    fetchRequest(args);
  }, []);


  return (
    <>{Object.keys(cocktail).length !== 0 ?
      <>
        <Container key={cocktail.id}>
          <Card style={{ width: '18rem' }}>
            <Card.Img variant="top" src={`http://localhost:5000/images${cocktail.imageUrl}`} />{/* image doesn't load*/}
            <Card.Body>
              <Card.Title>{cocktail.name}</Card.Title>
              <Card.Text>ingredients: {cocktail.ingredients.map(ingredient=> {
                return (
                  ingredient.name
                );
              }).join(', ')} <br />
              </Card.Text>
            </Card.Body>
          </Card>
          {cocktail.availableIn.length!==0 ?
            <h3>You can found it in: {cocktail.availableIn.map(bar=> {
              return (
                bar.name
              );
            }).join(', ')}
            </h3>:
            <h3>Not available at the moment :(</h3>}

          {isLoggedIn && user.role==='magician'?
            <Accordion defaultActiveKey="1" flush>
              <Accordion.Item eventKey="0">
                <Accordion.Button>
                  <h5>Add/Remove cocktail to/from the bar</h5>
                </Accordion.Button>
                <Accordion.Body>
                  <AddRemoveCocktail cocktailId={cocktail.id} setCocktail={setCocktail} />
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>:null}

          {isLoggedIn && user.role==='magician'?
            <Accordion defaultActiveKey="1" flush>
              <Accordion.Item eventKey="0">
                <Accordion.Button>
                  <h5>Update cocktail</h5>
                </Accordion.Button>
                <Accordion.Body>
                  <UpdateCocktail cocktailId={cocktail.id} setCocktail={setCocktail} />
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>:null}

          {isLoggedIn && user.role==='magician'?
            <Accordion defaultActiveKey="1" flush>
              <Accordion.Item eventKey="0">
                <Accordion.Button>
                  <h5>Add/Remove ingredients</h5>
                </Accordion.Button>
                <Accordion.Body>
                  <AddRemoveIngredients cocktailId={cocktail.id} ingredients={cocktail.ingredients} setCocktail={setCocktail} />
                </Accordion.Body>
              </Accordion.Item>
            </Accordion>:null}

          <Button onClick={back}>Back</Button>
        </Container>
      </>:
      <h3>Loading</h3>}
    </>
  );

};


ViewCocktailDetails.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
};


export default ViewCocktailDetails;
