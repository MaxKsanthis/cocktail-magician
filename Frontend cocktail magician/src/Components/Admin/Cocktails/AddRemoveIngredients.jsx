import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import fetchRequest from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';
import PropTypes from 'prop-types';

const AddRemoveIngredients = ({ cocktailId, ingredients, setCocktail }) => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({ ingredients: '' });

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const addIngredients = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);

    const args = {
      path: `cocktails/${cocktailId}/ingredients`,
      method: 'PUT',
      body: JSON.stringify(form),
      headers: {
        'Authorization': `Bearer ${getToken()}`,
        'Content-Type': 'application/json'

      },
      handler: ()=> {
        const argsForCocktail = {
          path: `cocktails/${cocktailId}`,
          handler: data =>{
            setCocktail(data);
            setField('ingredients', '');
          }
        };

        fetchRequest(argsForCocktail);
      }
    };

    fetchRequest(args);
  };

  const removeIngredients = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);

    const args = {
      path: `cocktails/${cocktailId}/ingredients`,
      method: 'DELETE',
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: ()=> {
        const argsForCocktail = {
          path: `cocktails/${cocktailId}`,
          handler: data =>{
            setCocktail(data);
            setField('ingredients', '');
          }
        };

        fetchRequest(argsForCocktail);

      } };

    fetchRequest(args);
  };

  return (
    <Form>
      <Form.Group className="mb-3" controlId="formPlaintext">
        <Form.Label>Ingredients</Form.Label>
        <Form.Control
          value={form.ingredients}
          required valid={ingredients.some((ingredient) => ingredient.name===form.ingredients)}
          placeholder="Ingredient 1, 2..." onChange={e => setField('ingredients', e.target.value.split(','))}
        />
        <Form.Control.Feedback type='invalid'>
          Ingredient doesn't exist!
        </Form.Control.Feedback>
      </Form.Group>

      <Button type="submit" onClick={addIngredients}>
        Add
      </Button>
      <Button type="submit" onClick={removeIngredients}>
        Remove
      </Button>
    </Form>
  );
};

AddRemoveIngredients.propTypes = {
  cocktailId: PropTypes.number,
  ingredients: PropTypes.array,
  setCocktail: PropTypes.function,


};


export default AddRemoveIngredients;
