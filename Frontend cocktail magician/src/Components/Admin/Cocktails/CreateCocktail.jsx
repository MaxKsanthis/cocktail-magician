import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import fetchRequest from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';

const CreateCocktail = () => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});
  const [ingredients, setIngredients] = useState([]);

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const createCocktail = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);
    const formData = new FormData();
    formData.append('name', form.name);
    form.ingredients.forEach(ingredient=> formData.append('ingredients', ingredient));
    formData.append('image', form.image);

    const args = {
      path: 'cocktails',
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: (response)=> {
        console.log(response);
        console.log(form);
      } };

    fetchRequest(args);
  };


  useEffect(() =>{
    const args = {
      path: 'ingredients',
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: (response)=> {
        setIngredients(response);
      } };

    fetchRequest(args);
  }, []);

  return (
    <Form className="form" createCocktail={createCocktail} validated={valid} noValidate autoComplete="off">
      <Form.Group className="mb-3" controlId="formBasicUsername">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" placeholder="Enter username" required minLength={2} maxLength={50} onChange={e => setField('name', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Name of the cocktail must be between 2-50 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid name! Name must be between 2-50 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      {ingredients.length ?
        <Form.Group className="mb-3" controlId="formPlaintext">
          <Form.Label>Ingredients</Form.Label>
          <Form.Control
            required valid={ingredients.some((ingredient) => ingredient.name===form.ingredients)}
            placeholder="Ingredient 1, 2..." onChange={e => {
              console.log(e.target.value.split(','));
              return setField('ingredients', e.target.value.split(', '));
            }}
          />
          <Form.Control.Feedback type='invalid'>
            Ingredient doesn't exist!
          </Form.Control.Feedback>
        </Form.Group> :
        <h3>Loading</h3>}

      <Form.Group controlId="formFile" className="mb-3">
        <Form.Label>Upload Image</Form.Label>
        <Form.Control
          type="file"
          onChange={e => setField('image', e.target.files[0])}
        />
      </Form.Group>

      <Button variant="dark" type="submit" onClick={createCocktail}>
        Create Cocktail
      </Button>
    </Form>
  );
};


export default CreateCocktail;
