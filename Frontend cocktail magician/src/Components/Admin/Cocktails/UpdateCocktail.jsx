import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import fetchRequest from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';
import PropTypes from 'prop-types';

const UpdateCocktail = ({ cocktailId, setCocktail }) => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const updateCocktail = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);
    const formData = new FormData();
    formData.append('name', form.name);
    formData.append('image', form.image);

    const args = {
      path: `cocktails/${cocktailId}`,
      method: 'PUT',
      body: formData,
      headers: {
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: ()=> {
        const argsForCocktail = {
          path: `cocktails/${cocktailId}`,
          handler: data =>{
            setCocktail(data);
          }
        };

        fetchRequest(argsForCocktail);
      }, };

    fetchRequest(args);
  };


  return (
    <Form className="form" createCocktail={updateCocktail} validated={valid} noValidate autoComplete="off">
      <Form.Group className="mb-3" controlId="formBasicUsername">
        <Form.Label>Name</Form.Label>
        <Form.Control type="text" placeholder="Enter username" required minLength={2} maxLength={50} onChange={e => setField('name', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Name of the cocktail must be between 2-50 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid name! Name must be between 2-50 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Form.Group controlId="formFile" className="mb-3">
        <Form.Label>Upload Image</Form.Label>
        <Form.Control
          type="file"
          onChange={e => setField('image', e.target.files[0])}
        />
      </Form.Group>

      <Button variant="dark" type="submit" onClick={updateCocktail}>
        Update Cocktail
      </Button>
    </Form>
  );
};

UpdateCocktail.propTypes = {
  cocktailId: PropTypes.number,
  setCocktail: PropTypes.function,
};

export default UpdateCocktail;
