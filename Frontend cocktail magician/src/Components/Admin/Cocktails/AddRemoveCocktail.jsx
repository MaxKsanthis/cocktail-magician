import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import fetchRequest from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';
import PropTypes from 'prop-types';

const AddRemoveCocktail = ({ cocktailId, setCocktail }) => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({ barName: '' });

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };


  const addCocktail = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);

    const argsForBar = {
      path: `bars?name=${form.barName}`,
      handler: (bar) => {
        if (bar.length!==0) {
          const args = {
            path: `bars/${bar[0].id}/cocktails/${cocktailId}`,
            method: 'PUT',
            headers: {
              'Authorization': `Bearer ${getToken()}`,
            }
          };
          const argsForCocktail = {
            path: `cocktails/${cocktailId}`,
            handler: data =>{
              setCocktail(data);
            }
          };

          fetchRequest(args);
          setTimeout(() => {
            fetchRequest(argsForCocktail);
            setField('barName', '');
          }, 20);
        };
      },
    };
    fetchRequest(argsForBar);
  };

  const removeCocktail = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);

    const argsForBar = {
      path: `bars?name=${form.barName}`,
      handler: (bar) => {
        if (bar.length!==0) {
          const args = {
            path: `bars/${bar[0].id}/cocktails/${cocktailId}`,
            method: 'DELETE',
            headers: {
              'Authorization': `Bearer ${getToken()}`,
            }
          };
          const argsForCocktail = {
            path: `cocktails/${cocktailId}`,
            handler: data =>{
              setCocktail(data);
            }
          };

          fetchRequest(args);
          setTimeout(() => {
            fetchRequest(argsForCocktail);
            setField('barName', '');
          }, 20);
        }

      },
    };
    fetchRequest(argsForBar);
  };
  return (
    <Form addCocktail={addCocktail} validated={valid} noValidate autoComplete="off">
      <Form.Group className="mb-3" controlId="formBasicCocktail">
        <Form.Control value={form.barName} type="text" placeholder="bar name" required minLength={5} maxLength={50} onChange={e => setField( 'barName', e.target.value )} />
        <Form.Control.Feedback type='invalid'>
          Invalid bar! Name of bar should be between 5 and 50 characters long!
        </Form.Control.Feedback>
      </Form.Group>

      <Button onClick={addCocktail}>
        Add
      </Button>
      <Button onClick={removeCocktail}>
        Remove
      </Button>
    </Form>
  );
};

AddRemoveCocktail.propTypes = {
  cocktailId: PropTypes.number,
  setCocktail: PropTypes.func,
};

export default AddRemoveCocktail;
