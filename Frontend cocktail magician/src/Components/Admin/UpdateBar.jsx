import React, { useState, useContext } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import fetchRequest from '../../requests/server-requests';
import { AuthContext, getToken } from '../../context/auth-context';
import PropTypes from 'prop-types';
import Accordion from 'react-bootstrap/esm/Accordion';

const UpdateBar = ({ id }) => {
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({});
  const { user, isLoggedIn } = useContext(AuthContext);

  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const onSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);
    console.log(form);
    const args = {
      path: `bars/${id}`,
      method: 'PUT',
      body: JSON.stringify(form),
      headers: {
        'Authorization': `Bearer ${getToken()}`,
        'Content-Type': 'application/json'
      },
      handler: (response)=> {
        console.log(response);
      } };

    fetchRequest(args);

  };

  return (
    <>{isLoggedIn && user.role==='magician'?
      <>
        <Accordion defaultActiveKey="1" flush>
          <Accordion.Item eventKey="0">
            <Accordion.Button>
              <h5>Update Bar</h5>
            </Accordion.Button>
            <Accordion.Body>
              <Form className="form" onSubmit={onSubmit} validated={valid} noValidate autoComplete="off">
                <Form.Group className="mb-3" controlId="formBasicUsername">
                  <Form.Label>Name</Form.Label>
                  <Form.Control type="text" placeholder="Enter username" required minLength={5} maxLength={50} onChange={e => setField('name', e.target.value)} />
                  <Form.Text className="text-muted">
                    Note: Name of the bar must be between 5-50 characters.
                  </Form.Text>
                  <Form.Control.Feedback type='invalid'>
                    Invalid name! Name must be between 5-50 characters long!
                  </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPhone">
                  <Form.Label>Phone</Form.Label>
                  <Form.Control type="phone" required minLength={2} maxLength={20} placeholder="Phone" onChange={e => setField('phone', e.target.value)} />
                  <Form.Text className="text-muted">
                    Note: Phone must be between 2-20 characters.
                  </Form.Text>
                  <Form.Control.Feedback type='invalid'>
                    Invalid phone! Phone must be between 2-20 characters long!
                  </Form.Control.Feedback>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicAddress">
                  <Form.Label>Address</Form.Label>
                  <Form.Control type="address" required minLength={5} maxLength={100} placeholder="Address" onChange={e => setField('address', e.target.value)} />
                  <Form.Text className="text-muted">
                    Note: Address must be between 5-100 characters.
                  </Form.Text>
                  <Form.Control.Feedback type='invalid'>
                    Invalid address! Address must be between 5-100 characters long!
                  </Form.Control.Feedback>
                </Form.Group>

                <Button variant="dark" type="submit" onClick={onSubmit}>
                  Update Bar
                </Button>
              </Form>


            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
      </>:
      null}
    </>
  );
};

UpdateBar.propTypes = {
  id: PropTypes.number,
};

export default UpdateBar;
