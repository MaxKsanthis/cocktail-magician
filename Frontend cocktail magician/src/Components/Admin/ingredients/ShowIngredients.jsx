import React from 'react';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import { useState, useEffect } from 'react';
import fetchRequest from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';
import CreateIngredient from './CreateIngredient';
import DeleteIngredient from './DeleteIngredient';

const ShowIngredients = () =>{

  const [ingredients, setIngredients] = useState([]);

  useEffect(() => {
    const args = {
      path: `ingredients`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: response => setIngredients(response)
    };

    fetchRequest(args);
  }, []);

  return (
    <Container>
      <CreateIngredient setIngredients={setIngredients} />
      {ingredients.length!==0?
        <Table className='users-table' striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>id</th>
              <th>ingredient</th>
              <th>used in cocktails</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {ingredients.map(ingredient =>{
              return (
                <tr key={ingredient.id}>
                  <td>{ingredient.id}</td>
                  <td>{ingredient.name}</td>
                  <th>{ingredient.cocktailsCount}</th>
                  <th>{ingredient.cocktailsCount===0 ?
                    <DeleteIngredient ingredientId={ingredient.id} ingredientName={ingredient.name} setIngredients={setIngredients} /> :
                    null}
                  </th>
                </tr>
              );
            })}
          </tbody>
        </Table> :
        <h3>Loading</h3>}
    </Container>
  );

};


export default ShowIngredients;
