import fetchRequest from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';
import React from 'react';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';

const DeleteIngredient = ({ ingredientId, ingredientName, setIngredients }) =>{
  const form ={ name: ingredientName };

  const onSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();

    const args = {
      path: `ingredients/${ingredientId}`,
      method: 'DELETE',
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`,
      }
    };

    const argsForIngredients = {
      path: `ingredients`,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: response => {
        setIngredients(response);
        console.log(response);

      }
    };


    fetchRequest(args);
    setTimeout(() => {
      fetchRequest(argsForIngredients);

    }, 20);
  };

  return (
    <Button onClick={onSubmit} variant="primary" type="submit">
      Delete
    </Button>
  );

};

DeleteIngredient.propTypes = {
  ingredientId: PropTypes.number,
  ingredientName: PropTypes.string,
  setIngredients: PropTypes.func,
};

export default DeleteIngredient;
