import fetchRequest from '../../../requests/server-requests';
import { getToken } from '../../../context/auth-context';
import React, { useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import PropTypes from 'prop-types';

const CreateIngredient = ({ setIngredients }) =>{
  const [valid, setValid] = useState(false);
  const [form, setForm] = useState({ name: '' });
  const setField = (field, value) => {
    setForm({
      ...form,
      [field]: value
    });
  };

  const onSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    setValid(!valid);

    const args = {
      path: 'ingredients',
      method: 'POST',
      body: JSON.stringify(form),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${getToken()}`,
      },
      handler: () => {
        const argsForIngredients = {
          path: `ingredients`,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${getToken()}`,
          },
          handler: response => {
            setIngredients(response);
            setField('name', '');
          }

        };

        fetchRequest(argsForIngredients);
      }
    };

    fetchRequest(args);
  };

  return (
    <Form onSubmit={onSubmit} validated={valid}>
      <Form.Group className="mb-3" controlId="formBasicUsername">
        <Form.Control value={form.name} type="text" placeholder="Ingredient" required minLength={2} maxLength={50} onChange={e => setField('name', e.target.value)} />
        <Form.Text className="text-muted">
          Note: Ingredient must be between 2-50 characters.
        </Form.Text>
        <Form.Control.Feedback type='invalid'>
          Invalid ingredient! Ingredient must be between 2-50 characters long!
        </Form.Control.Feedback>
      </Form.Group>
      <Button onClick={onSubmit} variant="primary" type="submit">
        Create
      </Button>
    </Form>
  );

};

CreateIngredient.propTypes = {
  setIngredients: PropTypes.func,
};

export default CreateIngredient;
