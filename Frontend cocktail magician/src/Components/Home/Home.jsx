import React, { useContext } from 'react';
import { AuthContext } from '../../context/auth-context';
import './Home.css';
import Container from 'react-bootstrap/Container';
import Carousel from 'react-bootstrap/Carousel';


const Home = () => {

  const { user, isLoggedIn } = useContext(AuthContext);

  return (
    <Container fluid className="welcome-msg">
      <h1>Welcome to Cocktail Magician!</h1>
      {isLoggedIn ? <h4>Hello {user.username}!</h4> : null}

      <Carousel fade>
        <Carousel.Item>
          <img
            src="https://images4.alphacoders.com/268/thumb-1920-268154.jpg" width="800" height="500"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            src="https://vedcdn.imgix.net/images/product/large/mixed-case-of-six-23124755.jpg?auto=compress,format" width="800" height="500"
            alt="Second slide"
          />

          <Carousel.Caption top>
            <h3>Here you will find amazing cocktails...</h3>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            // eslint-disable-next-line max-len
            src="https://lp-cms-production.imgix.net/features/2017/04/blindspot1-d8287f82de54.jpg?auto=format&fit=crop&sharp=10&vib=20&ixlib=react-8.6.4&w=850" width="800" height="500"
            alt="Third slide"
          />

          <Carousel.Caption>
            <h3>...made by magicians</h3>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>

      {/* <img src="https://images4.alphacoders.com/268/thumb-1920-268154.jpg" width="800" height="500" /> */}
    </Container>
  );
};

export default Home;
