import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types';

const SearchByName = ({ bars, routeChange }) =>{

  return (
    <Row>
      {bars.map(bar =>{
        return (
          <Col key={bar.id}>
            <Container>
              <Card style={{ width: '22rem' }}>
                <Card.Img variant="top" src={`http://localhost:5000/images${bar.url}`} />
                <Card.Body>
                  <Card.Title>{bar.name}</Card.Title>
                  <Card.Text>
                    Phone: {bar.phone} <br />
                    Address: {bar.address}
                  </Card.Text>

                  <Button onClick={() => routeChange(bar.id)}>
                    More...
                  </Button>
                </Card.Body>
              </Card>
            </Container>
          </Col>
        );
      })}
    </Row>
  );
};

SearchByName.propTypes = {
  bars: PropTypes.array,
  routeChange: PropTypes.func,
};

export default SearchByName;
