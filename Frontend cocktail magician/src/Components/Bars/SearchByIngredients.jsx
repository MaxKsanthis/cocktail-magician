import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import PropTypes from 'prop-types';

const SearchByIngredients = ({ bars, routeChange }) =>{
  return (
    <Row>
      {bars.map(barsForEachIngredient=>barsForEachIngredient.map(bar =>{
        return (
          <Col key={bar.barId}>
            {console.log(bar)}
            <Container>
              <Card style={{ width: '22rem' }}>
                <Card.Img variant="top" src={`http://localhost:5000/images${bar.cocktailImage}`} />
                <Card.Body>
                  <Card.Title>{bar.cocktailName} in {bar.barName}</Card.Title>
                  <Card.Text>
                    Phone: {bar.phone} <br />
                    Address: {bar.address}
                  </Card.Text>

                  <Button onClick={() => routeChange(bar.barId)}>
                    More...
                  </Button>
                </Card.Body>
              </Card>
            </Container>
          </Col>
        );
      }))}
    </Row>
  );
};

SearchByIngredients.propTypes = {
  bars: PropTypes.array,
  routeChange: PropTypes.func,
};

export default SearchByIngredients;
