import React, { Fragment } from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import fetchRequest from '../../requests/server-requests';
import UpdateBar from '../Admin/UpdateBar';
import ShowReview from '../User/reviews/ShowReview';
import Row from 'react-bootstrap/Row';

const ViewBarDetails = (props) =>{
  const id = props.match.params.id;
  const [bar, setBar] = useState({});
  const back = () => props.history.goBack();

  useEffect(() =>{
    const args = {
      path: `bars/${id}`,
      headers: {
        'Content-Type': 'application/json',
      },
      handler: data =>{
        setBar(data);
      }
    };

    fetchRequest(args);
  }, []);


  return (
    <>{Object.keys(bar).length !== 0 ?
      <>
        <Container key={bar.id}>
          <Card style={{ width: '22rem' }}>
            <Card.Img variant="top" src={`http://localhost:5000/images${bar.images[0].url}`} />{/* image doesn't load*/}
            <Card.Body>
              <Card.Title>{bar.name}</Card.Title>
              <Card.Text>{bar.address} <br /> {bar.phone}</Card.Text>
            </Card.Body>
          </Card>
          <div>Reviews: {bar.reviewsCount} | Rating: {bar.rating ? (bar.rating) : ('No ratings')}</div>
          <div>List of cocktails: {
            bar.cocktails.length!==0 ?
              <Row>
                {bar.cocktails.map(cocktail=>{
                  return (
                    <Fragment key={cocktail.id}>
                      <Card style={{ width: '12rem' }}>
                        <Card.Img variant="top" src={`http://localhost:5000/images${cocktail.imageUrl}`} />{/* image doesn't load*/}
                        <Card.Body>
                          <Card.Title>{cocktail.name}</Card.Title>
                        </Card.Body>
                      </Card>
                    </Fragment>
                  );
                })}
              </Row>:<h4>No cocktails at the moment</h4>
          }
          </div>
          <UpdateBar id={bar.id} />

          <Button onClick={back}>Back</Button>
        </Container>
        <ShowReview id={id} />

      </>:
      <h3>Loading</h3>}
    </>
  );

};


ViewBarDetails.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
};


export default ViewBarDetails;
