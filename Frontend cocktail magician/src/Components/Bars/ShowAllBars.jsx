import React from 'react';
import Container from 'react-bootstrap/Container';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import fetchRequest from '../../requests/server-requests';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import SearchByName from './SearchByName';
import SearchByIngredients from './SearchByIngredients';
import { AuthContext } from '../../context/auth-context';
import Accordion from 'react-bootstrap/esm/Accordion';
import CreateBar from '../Admin/CreateBar';

const ShowAllBars = ({ history }) =>{
  const [state, setState]=useState('start');
  const [bars, setBars]=useState([]);
  const [barsForIng, setBarsForIng]=useState([]);

  const { user, isLoggedIn } = useContext(AuthContext);

  const [input, setInput] = useState({ name: '' });

  const setField = (field, value) => {
    setInput({
      ...input,
      [field]: value
    });
  };

  useEffect(() =>{
    const args = {
      path: `bars?name`,
      handler: data =>{
        setBars(data);
      }
    };

    fetchRequest(args);
  }, []);

  const searchByName = () =>{
    const args = {
      path: `bars?name=${input.name}`,
      handler: data =>{
        setBars(data);
        setField('name', '');
      }
    };

    fetchRequest(args);

    setState('byName');

  };


  const searchByIngredients = () =>{
    const args = {
      path: `bars/cocktail-ingredients?${(input.name.split(', ').map(ingredient=>{
        return (
          `values=${ingredient}`
        );
      }
      ).join('&'))}`,
      handler: data =>{
        setBarsForIng(data.map(ing=>{
          return ing.bars;
        }
        ));
        setField('name', '');
      }
    };

    fetchRequest(args);

    setState('byIngredients');
  };

  const routeChange = (id) =>{
    const path = `/bars/${id}`;
    history.push(path);
  };

  return (
    <Container>
      <InputGroup className="mb-3">
        <FormControl
          value={input.name}
          aria-label="Text input with dropdown button"
          placeholder="Search Bar"
          aria-describedby="basic-addon2"
          onChange={e => setField('name', e.target.value)}
        />

        <DropdownButton
          variant="outline-secondary"
          title="Search"
          id="input-group-dropdown-2"
          align="end"
        >
          <Dropdown.Item onClick={searchByName} href="#">by name</Dropdown.Item>
          <Dropdown.Item onClick={searchByIngredients} href="#">by ingredients</Dropdown.Item>
        </DropdownButton>
      </InputGroup>

      {isLoggedIn && user.role==='magician'?
        <Accordion defaultActiveKey="1" flush>
          <Accordion.Item eventKey="0">
            <Accordion.Button>
              <h5>Create bar</h5>
            </Accordion.Button>
            <Accordion.Body>
              <CreateBar />
            </Accordion.Body>
          </Accordion.Item>
        </Accordion> :
        null}

      {state==='start'&&(
        <Row>
          {bars.map(bar =>{
            return (
              <Col key={bar.id}>
                <Container>
                  <Card style={{ width: '22rem' }}>
                    <Card.Img variant="top" src={`http://localhost:5000/images${bar.url}`} />
                    <Card.Body>
                      <Card.Title>{bar.name}</Card.Title>
                      <Card.Text>
                        Phone: {bar.phone} <br />
                        Address: {bar.address}
                      </Card.Text>

                      <Button onClick={() => routeChange(bar.id)}>
                        More...
                      </Button>
                    </Card.Body>
                  </Card>
                </Container>
              </Col>
            );
          })}
        </Row>
      )}
      {state==='byName'&&(
        <SearchByName bars={bars} routeChange={routeChange} />
      )}
      {state==='byIngredients'&&(
        <SearchByIngredients bars={barsForIng} routeChange={routeChange} />
      )}
    </Container>
  );

};

ShowAllBars.propTypes = {
  history: PropTypes.object,
};


export default ShowAllBars;
