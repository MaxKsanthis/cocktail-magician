/* eslint-disable require-jsdoc */
import './App.css';
import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavigationBar from './Navigation/Navbar.jsx';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Home from './Components/Home/Home';
import Register from './Components/Register/Register';
import LogIn from './Components/LogIn/LogIn';
import { AuthContext, getUser, getDecodedUser } from './context/auth-context';
import PropTypes from 'prop-types';
import ShowAllBars from './Components/Bars/ShowAllBars';
import ViewMyProfile from './Components/User/ViewMyProfile';
import ViewOtherProfile from './Components/User/ViewOtherProfile';
import ViewBarDetails from './Components/Bars/ViewBarDetails';
import ShowIngredients from './Components/Admin/ingredients/ShowIngredients';
import ShowAllCocktails from './Components/Cocktails/ShowAllCocktails';
import ViewCocktailDetails from './Components/Cocktails/ViewCocktailDetails';

const PrivateRoute = ({ component: Component, auth, ...rest }) => {
  return (
    <Route
      {...rest} render={(props) => {
        return auth ?
          <Component {...props} /> :
          <Redirect to='/login' />;
      }}
    />
  );
};

// eslint-disable-next-line func-style
function App() {
  const user = getDecodedUser();
  const [auth, setAuth] = useState({
    user,
    isLoggedIn: !!user
  });

  useEffect(() => {
    getUser()
      .then(setAuth)
      .catch(err => console.error(err));
  }, []);

  return (
    <AuthContext.Provider value={{ ...auth, setAuth }}>
      <BrowserRouter>
        <NavigationBar />
        <Switch>
          <Route path="/home" component={Home} />
          <Route path="/profile" exact component={ViewMyProfile} />
          <Route path="/profile/:id" component={ViewOtherProfile} />
          <Route path="/ingredients" component={ShowIngredients} />
          <Route path="/bars" exact component={ShowAllBars} />
          <Route path="/bars/:id" component={ViewBarDetails} />
          <Route path="/register" component={Register} />
          <Route path="/cocktails" exact component={ShowAllCocktails} />
          <Route path="/cocktails/:id" component={ViewCocktailDetails} />

          <Route exact path="/">
            <Redirect to={auth?.isLoggedIn ? '/home' : '/login'} />
          </Route>
          <Route path="/login" component={LogIn} />
        </Switch>
      </BrowserRouter>
    </AuthContext.Provider>
  );
}

PrivateRoute.propTypes = {
  component: PropTypes.func,
  auth: PropTypes.bool
};
export default App;
